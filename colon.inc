%define next_entry 0

%macro colon 2
%ifid %2
	%2: dq next_entry
%else
	%error "Second argument must be an id"
%endif
%ifstr %1
	db %1, 0
%else
	%error "First argument must be a string"
%endif

%define next_entry %2
%endmacro
