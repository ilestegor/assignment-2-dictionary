import subprocess as sb
from subprocess import PIPE
inputs = "tests/input.txt"
outputs = "tests/output.txt"
codes = "tests/codes.txt"
executable = "./dict"

inputs_file = open(inputs, 'r')
outputs_file = open(outputs, 'r')
codes_file = open(codes, 'r')

result = True

def print_res(input_string, expected_value, received_value):
    print("Test with input" + " [" + input_string + "] " + "FAILED")
    print("Excpected value: " + expected_value)
    print("Got value: " + received_value)
    print("------------------------------\n")


for input_str, output_str, returned_code in zip(inputs_file, outputs_file, codes_file):
    res = sb.Popen(executable, universal_newlines=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    data = res.communicate(input_str.strip())
    out = data[0].strip()
    err = data[1].strip()
    code = res.returncode
    exp_value = output_str.strip()
    input_str = input_str.strip()
    exp_code = int(returned_code.strip())
    
    if out != "" and (out != exp_value or code != exp_code):
        print_res(input_str, exp_value, out)
        result = False
    elif err != "" and (err != exp_value or code != exp_code):
        print_res(input_str, exp_value, err)
        result = False
if result:
    print("All test are passed!")
