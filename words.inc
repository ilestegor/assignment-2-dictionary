%include "colon.inc"
section .rodata

colon "i just", help
db "wanna be happy", 0

colon "why cant i be happy", why
db "because assembly eats your soul", 0 

colon "apple_pie_butterfly_sunshine", happy_days
db "moonlight_stars_sparkling_water", 0

colon "mountain_river_forest_sunset", nature_love
db "ocean_waves_whispering_wind", 0

colon "fireflies_buttercups_rainbow", serenity
db "chocolate_marshmallows_smiles", 0

colon "laughter_happiness_kindness_love", harmony
db "melody_giggles_dancing_dreams", 0

colon "garden_blossoms_sunflowers_bees", peace
db "adventure_wanderlust_discover", 0

