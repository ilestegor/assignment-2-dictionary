%include "words.inc"
%include "lib.inc"
%include "dict.inc"
%define BUFFER_SIZE 256

%macro print_and_exit 2
	call %1
	mov rdi, %2
	call exit
%endmacro

section .bss
buffer resb BUFFER_SIZE ;reserve space for buffer

section .rodata
read_error_msg db "Error while reading from stdin", `\n`, 0
not_found_error_msg db "Key was not found in dictionary", `\n`, 0

section .text
	global _start
_start:
.cycle:
	mov rdi, buffer
	mov rsi, 255 ;taking into account null-termination
	call read_string
	test rax, rax
	jz .read_error

	mov rdi, rax
	mov rsi, next_entry
	call find_word
	test rax, rax
	jz .not_found
	
	;if found -> stdout value by key
	mov rdi, rax
	call get_value
	mov rdi, rax
	print_and_exit print_string, 0
.read_error:
	mov rdi, read_error_msg
	print_and_exit print_error, 1
.not_found:
	mov rdi, not_found_error_msg
	print_and_exit print_error, 1
	
