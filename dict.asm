%include "lib.inc"
global find_word
global get_value
%define QWORD_SIZE 8


;macro for popping values
%macro multipop 1-*
%rep %0
	%rotate -1
	pop %1
%endrep
%endmacro


section .text
;Принимает указатель на нуль-терминированную строку и указатель на начало словаря
;Проходится по всему словарю в поисках подходящего ключа. Если найден -> вовзрат адрес начала вхождения в словаре
;Иначе -> возвращает 0
;rdi - указатель на нуль-терминированную строку
;rsi - указатель на начало словаря
;rax - адрес начала вхождения в словаре

;pointer is 8 bytes
; so we need to inc ptr to the start if dict by 8 bytes to iterate over it, after every inc of rsi we need to compare [rdi] with [rsi + 8] 
find_word:
	xor rax, rax
	push r12
	push r13
	mov r12, rdi
	mov r13, rsi
	
.loop:
	test r13, r13 ;check if we got to the end of dict
	je .not_found

	lea rsi, [r13 + QWORD_SIZE] ;mov didnt work :(
	mov rdi, r12
	call string_equals
	test rax, rax 
	jne .found ;if rax = 1 then found
	mov r13, [r13] ;move next entry pointer
	jmp .loop
		
.found:
	mov rax, r13
	multipop r12, r13
	ret
.not_found:
	xor rax, rax
	multipop r12, r13
	ret

;Получение значения по ключу
;rdi - Адрес вхождения в словарь
;rax - Значение в словаре по ключу
get_value:
	lea rdi, [rdi + QWORD_SIZE] ;get key
	push rdi
	call string_length ;find key`s length
	pop rdi
	lea rax, [rdi + rax + 1] ; sum key address with its length to get to the value, plus 1 because string_lnegth does not count null-terminator
	ret
	
		
	 	
