ASM=nasm
EFLAGS=-f elf64 -g

.PHONY: all clean test
all: dict

%.o: %.asm
	$(ASM) $(EFLAGS) -o $@ $<

dict:main.o lib.o dict.o
	ld -o $@ $^
test: dict
	python3 tests/test.py
clean:
	$(RM) dict *.o
